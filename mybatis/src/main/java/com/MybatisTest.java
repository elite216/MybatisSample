package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan(basePackages= {"com"})
public class MybatisTest {
	
	public static void main(String [] args) {
		SpringApplication.run(MybatisTest.class, args);
	}
}
