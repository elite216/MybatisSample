package com;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
@Mapper
public interface Repository {
	@Select("select * from [user]")
	public List <UserEntity> findAll();
}
